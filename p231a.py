# -*- coding: utf-8 -*-
"""
Created on Tue May 15 11:07:26 2018

@author: mishrap
"""

N = int(input())

num_sols = 0
for _ in range(N):
    list_sure = [1  for x in input().split() if x == '1']
    if len(list_sure) >= 2:
        num_sols += 1
print(num_sols)