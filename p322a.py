# -*- coding: utf-8 -*-
"""
Created on Thu May 17 11:43:29 2018

@author: mishrap
"""

n, m = map(int, input().split())

n_list = list(range(1, n+1))
m_list = list(range(1, m+1))

out_list = []
for i, x in enumerate(n_list[:]):
    for y in m_list:
        out_list.append((x, y))
print(out_list)